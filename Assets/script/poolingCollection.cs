﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class poolingCollection : MonoBehaviour
{

    public static poolingCollection current;

    [SerializeField] private Transform objParent;

    public int amountLights = 7;

    public GameObject lightPrefab;

    private List<GameObject> pooledLights;

    private void Awake()
    {
        current = this;
    }

    private void Start()
    {
        pooledLights = new List<GameObject>();
        for (int  i = 0;  i < amountLights; i++)
        {
            GameObject light = (GameObject) Instantiate(lightPrefab);
            
            light.transform.SetParent(objParent);
            light.SetActive(false);
            
            pooledLights.Add(light);
        }
    }

    private GameObject poolingLights()
    {
        GameObject light = (GameObject) Instantiate(lightPrefab);
            
        light.transform.SetParent(objParent);
        light.SetActive(false);
        pooledLights.Add(light);
        return light;
    }

    public GameObject getPooledLights()
    {
        for (int i = 0; i < pooledLights.Count; i++)
        {
            if (!pooledLights[i].activeInHierarchy)
            {
                return pooledLights[i];
            }
        }

       return poolingLights();
        
    }
}
