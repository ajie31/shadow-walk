﻿
using UnityEngine;
using UnityEngine.EventSystems;


public class playerMove : MonoBehaviour
{
    public float movingSpeed = 0;
   // public float reduceCounter;
    public GameObject btt1;
    
    private CharacterController2D controller;
    private swipeGesture sg;
    private bool jump = false;
   // private bool crouch = false;
   // private float counterIt;
    
   // [SerializeField]private DynamicLight normalLight; 
   // [SerializeField]private DynamicLight occlusion;
    //private bool isPointer = false;
    private Rigidbody2D rb;

    private void Start()
    {
        controller = GetComponent<CharacterController2D>();
        sg = GetComponent<swipeGesture>();
        rb = GetComponent<Rigidbody2D>();
       // counterIt = reduceCounter;
        var pointer = new PointerEventData(EventSystem.current); 

            
        ExecuteEvents.Execute(btt1, pointer, ExecuteEvents.pointerUpHandler);
 
    }



    private void FixedUpdate()
    {
       
           // reduceCounter -= Time.fixedDeltaTime;

        controller.Move(movingSpeed * Time.fixedDeltaTime,sg.crouch,false);

      /*  if (reduceCounter <= 0 && occlusion.LightRadius >= 1)
        {
            normalLight.LightRadius -= 1f; 
            occlusion.LightRadius -= 0.25f;
            reduceCounter = counterIt;
        }*/
    }
    public void OnLanding()
    {
        
        jump = false;
    }
    
    public void forceOn()
    {
        if (!jump)
        {
            //animator.SetBool("jumping",true);
            rb.AddRelativeForce(new Vector2(0,10f),ForceMode2D.Impulse);
            jump = true;
        }
        
        //isPointer = true;

    }   
    public void slideOfforJump()
    {
        if (!sg.crouch)
        {
            forceOn(); 
        }

     
        
    }

}
