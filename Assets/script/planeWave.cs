﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class planeWave : MonoBehaviour
{
 [Range(0.1f, 28.0f)] public float heightScale = 2.0f;
 [Range(0.1f, 70.0f)] public float detailScale = 23.0f;
 [SerializeField] private generateMesh _generate;
 
 private Mesh waveMesh;
 private Vector3[] vertices;
 


 public void generateWave(GameObject obj)
 {
 //Matrix4x4 localToWorld = transform.localToWorldMatrix;
  waveMesh = obj.GetComponent<MeshFilter>().mesh;
  vertices = waveMesh.vertices;
  
  int counter = 0;
  int yLevel = 0;

  for (int i = 0; i < _generate.xSize; i++)
  {
   for (int j = 0; j < _generate.zSize; j++)
   {
    
    CalculationMethod(counter, yLevel,obj);//changing vert here 
    counter++;
    
   }

   yLevel++;

  }

  
 
  //waveMesh.Clear();
  waveMesh.vertices = vertices;
 
  waveMesh.RecalculateBounds();
  waveMesh.RecalculateNormals();
 /* DestroyImmediate(obj.GetComponent<MeshCollider>());

  MeshCollider collider = obj.AddComponent<MeshCollider>();
  collider.sharedMesh = null;
  
  collider.sharedMesh = waveMesh;*/
  
  

 }
 
 public Vector3 GetVertexWorldPosition(Vector3 vertex, Transform owner)
 {
  return owner.localToWorldMatrix.MultiplyPoint3x4(vertex);
 }

 void CalculationMethod(int i, int j,GameObject obj)
 {
 
  
   vertices[i].y = Mathf.PerlinNoise(    
                    (vertices[i].x + obj.transform.position.x) / detailScale,
                    (vertices[i].z + obj.transform.position.z) / detailScale)
                   * heightScale;
   
   vertices[i].y += j;
  
 }
}
