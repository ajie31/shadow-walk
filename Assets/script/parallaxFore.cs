﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parallaxFore : MonoBehaviour
{
    private float length, startPos1,startPos2,temp,dist;

    public GameObject cam;

    private List<GameObject> foreGround;
    private int rightSide;
    private float rangeGantung;
    [SerializeField] private Sprite newleft;
    [SerializeField] private Sprite newmid;
    [SerializeField] private Sprite newright;
    
    private GameObject gantungLurus;
    private GameObject gantungDy;
    private GameObject gantungDy1;
    private SpriteRenderer _renderer1;
    private SpriteRenderer _renderer2;
    public float parallaxEffects;
    
    private float random;

    // Start is called before the first frame update
    void Start()
    {
        
        foreGround = new List<GameObject>();
        rightSide = 1;
        foreGround.Add(transform.GetChild(0).gameObject);
        foreGround.Add(transform.GetChild(1).gameObject);
        rangeGantung = Random.Range(1, 2);
       _renderer1 = foreGround[0].GetComponent<SpriteRenderer>();
        _renderer2 = foreGround[1].GetComponent<SpriteRenderer>();
        _renderer1.sprite = newleft;
        _renderer1.sprite = newmid;
        _renderer2.sprite = newright;
        gantungDy = foreGround[0].transform.GetChild(0).gameObject;
        gantungDy1 = foreGround[1].transform.GetChild(0).gameObject;
        gantungLurus = foreGround[1].transform.GetChild(2).gameObject;
        startPos1 = foreGround[0].transform.position.x;
        startPos2 = foreGround[1].transform.position.x;
        length = foreGround[1].GetComponent<SpriteRenderer>().bounds.size.x;
        temp = cam.transform.position.x * (1 - parallaxEffects);
        dist = cam.transform.position.x * parallaxEffects;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        temp = cam.transform.position.x * (1 - parallaxEffects);
        dist = cam.transform.position.x * parallaxEffects;

        foreGround[0].transform.position = new Vector3(startPos1 + dist, transform.position.y, transform.position.z);
        foreGround[1].transform.position = new Vector3(startPos2 + dist, transform.position.y, transform.position.z);

        if (temp > startPos1 + length)
        {

            gantungDy.SetActive(false);
            
            random = Random.value;
            if (rightSide == 1)
            {
                _renderer1.sprite = newleft;
                rightSide = 0;
                if (random > .7f)
                {
                    rangeGantung = Random.Range(4, 7);
                    gantungDy.transform.localPosition = new Vector3(rangeGantung,-.95f,0);
                    gantungDy.SetActive(true);
                }
            }
            else
            {
                if (random > .3f)
                {
                    _renderer1.sprite = newmid;
                    rightSide = 0;
                }
                else
                {
                    _renderer1.sprite = newright;
                    rightSide = 1;
                    if (random < .7f)
                    {
                        rangeGantung = Random.Range(-8, -4);
                        gantungDy.transform.localPosition = new Vector3(rangeGantung,-0.95f,0);
                        gantungDy.SetActive(true);
                    }
                }
            }
            startPos1 += length * 2;
        }

        if (temp > startPos2 + length)
        {
            gantungDy1.SetActive(false);
            gantungLurus.SetActive(false);
            random = Random.value;
            if (rightSide == 1)
            {
                _renderer2.sprite = newleft;
                rightSide = 0;
                if (random > .9f)
                {
                        
                    rangeGantung = Random.Range(-8, 7);
                    gantungLurus.transform.localPosition = new Vector3(rangeGantung,-0.38f,0);
                    gantungLurus.SetActive(true);
                }
            }
            else
            {
                if (random > .3f)
                {
                   
                    _renderer2.sprite = newmid;
                    rightSide = 0;
                    if (random > .7f)
                    {
                        
                        rangeGantung = Random.Range(-8, 7);
                        gantungDy1.transform.localPosition = new Vector3(rangeGantung,-.95f,0);
                        gantungDy1.SetActive(true);
                    }
                }
                else
                {
                    _renderer2.sprite = newright;
                    rightSide = 1;
                    if (random > .7f)
                    {
                        rangeGantung = Random.Range(-8, -4);
                        gantungDy1.transform.localPosition = new Vector3(rangeGantung,-.95f,0);
                        gantungDy1.SetActive(true);
                    }
                    if (random > .9f)
                    {
                        
                        rangeGantung = Random.Range(-8, 7);
                        gantungLurus.transform.localPosition = new Vector3(rangeGantung,-0.38f,0);
                        gantungLurus.SetActive(true);
                    }
                }
            }
            startPos2 += length * 2;   
        }
    }
}
