﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parallaxBackground : MonoBehaviour
{
    private float length, startPos1,startPos2, temp,dist;
        
    public GameObject cam;
    
    private List<GameObject> bacground;
        
    public float parallaxEffects;
    // Start is called before the first frame update
    void Start()
    {
        bacground = new List<GameObject>();
        bacground.Add(transform.GetChild(0).gameObject);
        bacground.Add(transform.GetChild(1).gameObject);
        startPos1 = bacground[0].transform.position.x;
        startPos2 = bacground[1].transform.position.x;
        length = bacground[1].GetComponent<SpriteRenderer>().bounds.size.x - .3f;
        temp = cam.transform.position.x * (1 - parallaxEffects);
        dist = cam.transform.position.x * parallaxEffects;
        
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        temp = cam.transform.position.x * (1 - parallaxEffects);
        dist = cam.transform.position.x * parallaxEffects;
        
        bacground[0].transform.position = new Vector3(startPos1 + dist,transform.position.y,transform.position.z);
        bacground[1].transform.position = new Vector3(startPos2 + dist,transform.position.y,transform.position.z);

        if (temp > startPos1 + length)
        {
            startPos1 += length * 2;
        }
        if (temp > startPos2 + length)
        {
            startPos2 += length * 2;
        }
       /* else if (temp < startPos - length)
        {
            startPos -= length;
        }*/
    }
}
