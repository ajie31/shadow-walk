﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMove : MonoBehaviour
{
   
    [SerializeField] private Transform player;
    [SerializeField] private Transform light;
    
    private Vector3 startOffset;
    private Vector3 plusx;
    private Vector3 cameraM;
    // Start is called before the first frame update
    void Start()
    {
        plusx = new Vector3(1f,0,light.position.z);
        startOffset = transform.position - new Vector3(player.position.x,0,0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        cameraM = new Vector3(player.position.x,0)+ startOffset;
        light.transform.position = player.position + plusx;
        transform.position = cameraM;
    }
}
