﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class swipeGesture : MonoBehaviour
{

    private bool tap, swipeLeft, swipeUp, swipeRight, swipeDown;
    public bool isDragging = false;
    public bool crouch = false;
    private Vector2 startTouch, swipeDelta;

    
    public Vector2 _swipeDelta
    {
        get { return swipeDelta; }
    }
    
    public bool _swipeLeft
    {
        get { return swipeLeft; }
    }
    public bool _swipeUp
    {
        get { return swipeUp; }
    }
    public bool _swipeRight
    {
        get { return swipeRight; }
    }
    public bool _swipeDown
    {
        get { return swipeDown; }
    }
    
    
    // Update is called once per frame
    void Update()
    {
        tap = swipeLeft = swipeUp = swipeRight = swipeDown = false;

        #region Standalone Inputs

        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            isDragging = true;
            startTouch = Input.mousePosition;
        }
        
        else if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
            
            Reset();
        }

        #endregion

        #region Mobile Inputs

        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                
                tap = true;
                isDragging = true;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDragging = false;
                Reset();
            }

        }


        #endregion
        
        swipeDelta = Vector2.zero;
        if (isDragging)
        {
            if (Input.touches.Length > 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;
            }
            else if (Input.GetMouseButton(0))
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        if (swipeDelta.magnitude > 125)
        {
            float x = swipeDelta.x;
            float y = swipeDelta.y;

            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                {
                    swipeLeft = true;
                }

                else
                {
                    swipeRight = true;
                }
             
            }
            else
            {
                if (y < 0 && !crouch)
                {
                    //Debug.Log("here");
                    crouch = true;
                    Invoke("setCrouch",.5f);
                    swipeDown = true;
                }

                else
                {
                    swipeUp = true;
                }
            }

        }

    }

    private void Reset()
    {
        startTouch = swipeDelta = Vector2.zero;
        isDragging = false;
       
    }

    private void setCrouch()
    {
        crouch = false;
    }

}
