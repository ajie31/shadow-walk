﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class caching : MonoBehaviour
{
    public collidder _col;
    // Start is called before the first frame update
    void Start()
    {
        Timing.RunCoroutine(waitFor());
    }

    IEnumerator<float> waitFor()
    {
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(_col.release()));
        yield return Timing.WaitForSeconds(2);
        transform.gameObject.SetActive(false);
    }
    

}
