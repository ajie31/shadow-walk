﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class poolingLand : MonoBehaviour
{
    public static poolingLand current;

    [SerializeField] private Transform landParent;

    public int amountLand = 7;
    public int amountRope = 5;
    public bool ready = false;
    public GameObject landPrefab;
    public GameObject landRope;
    public GameObject landTample;
    private List<GameObject> pooledLand;
    private List<GameObject> pooledRope;
    private List<GameObject> pooledBigTample;
    private int counter = 0;
    private void Awake()
    {
        Application.targetFrameRate = 30;
        current = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        pooledLand = new List<GameObject>();
        for (int i = 0; i < amountLand; i++)
        {
            GameObject land = (GameObject)Instantiate(landPrefab);
            land.transform.SetParent(landParent);
            land.SetActive(false);
            
            pooledLand.Add(land);
        }
        pooledRope = new List<GameObject>();
        
        for (int i = 0; i < amountRope; i++)
        {
            GameObject rope = (GameObject)Instantiate(landRope);
            rope.transform.SetParent(landParent);
            rope.SetActive(false);
            
            pooledRope.Add(rope);
        }
        
        pooledBigTample = new List<GameObject>();
        
        for (int i = 0; i < amountRope; i++)
        {
            GameObject land = (GameObject)Instantiate(landTample);
            land.transform.SetParent(landParent);
            land.SetActive(false);
            
            pooledBigTample.Add(land);
        }

        ready = true;
        Timing.RunCoroutine(falseDelay());
    }

    public GameObject getPooledLand()
    {
        for (int i = 0; i < pooledLand.Count ; i++)
        {
            if (!pooledLand[i].activeInHierarchy)
            {
                return pooledLand[i];
            }
        }

        return null;
    }
    
    public GameObject getPooledRope()
    {
        for (int i = 0; i < pooledRope.Count ; i++)
        {
            if (!pooledRope[i].activeInHierarchy)
            {
                return pooledRope[i];
            }
        }

        return null;
    }
    public GameObject getPooledBigTample()
    {
        for (int i = 0; i < pooledBigTample.Count ; i++)
        {
            if (!pooledBigTample[i].activeInHierarchy)
            {
                return pooledBigTample[i];
            }
        }

        return null;
    }

    public void addToList(GameObject obj)
    {
        pooledLand.Add(obj);
    }
    private IEnumerator<float> falseDelay()
    {
        yield return Timing.WaitForSeconds(1f);
        while (true)
        {
            pooledRope[counter].SetActive(false);
            
            
            if (counter == pooledRope.Count -1)
            {
                counter = 0;
                break;
            }
            counter++;
            yield return Timing.WaitForSeconds(0.3f);
        }
        
        
    }
}
