﻿
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using MEC;



public class gameplayManager : MonoBehaviour
{

    public static bool restartGame = false;
    [SerializeField] private GameObject overlay;
    
    public RectTransform shopOrPauseOverlay;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI PauseText;
    public Color colorRestart;
    [SerializeField] private RectTransform newParent;
    [SerializeField] private RectTransform MainMenuButton;
    [SerializeField] private RectTransform resumeButton;
    [SerializeField] private RectTransform restartButton;
    [SerializeField] private CanvasGroup cgMainMenu;
    [SerializeField] private RectTransform mainMenu;
    [SerializeField] private GameObject exitMenu;
    private Transform _playerT;
   
    private playerMove _move;
    private Rigidbody2D rb;
    private bool tomainMenu = false;
    private bool pauseStatus = false;
    //private Vector2 panelTemp;
    private Scene _scene;
    private int _scoreMin = 5;
    private int countDown = 0;
    private int _currentPlayerPost = 0;
    private mainMenu _menu;
    private float alphaValStart;
    [HideInInspector]public CanvasGroup cg;
    [HideInInspector]  public int score = 0;
    [HideInInspector]  public float panelMoveH;
    [HideInInspector]  public float zeroSet;
    [HideInInspector]  public float panelCurrentH;
    [HideInInspector]  public float _timeStartedLerping ;
    [HideInInspector]  public float timing = 0;
    [HideInInspector]  public float timeSinceStarted;
    [HideInInspector]  public float currentVal;
    [HideInInspector]  public bool gameOver = false;
    [HideInInspector]  public bool once = false;
    
    public Vector2[] offsetUI;
    //0 zero condition,1 pause menu overlay offset
    
    public Vector2[] anchors;
    //****0-1 resume&shop button , 2 - 3 main menu& leaderBoard button,6-9 text pause / title,10-13 restart button,14-15 score Text,16-17 pause menu****
    // 0-1, resume button anchors
    //  2-3 main menu button anchors (pause Menu), 4-5 main menu button (end menu)
    // 6-7 text pause (pause), 8 - 9 text countdown pause
    // 10-11 restart button(pause), 12-13 restart endGame
    //14 -15 score text (endGame)
    // 16-17 pause menu overlay (pause)
    
    
    private void Start()
    {
        _playerT = GameObject.FindWithTag("Player").transform;
        _move = _playerT.GetComponent<playerMove>();
        rb = _playerT.GetComponent<Rigidbody2D>();
        cg = overlay.GetComponent<CanvasGroup>();
        _menu = GetComponent<mainMenu>();
        _scene = SceneManager.GetActiveScene();
        alphaValStart = cg.alpha;
        _scoreMin = (int)_playerT.position.x;
        panelCurrentH = shopOrPauseOverlay.offsetMax.x;
        _timeStartedLerping = Time.time;
        timeSinceStarted = Time.time - _timeStartedLerping;
        timing = timeSinceStarted;
        zeroSet =0;
        panelMoveH =0; 
        _currentPlayerPost = (int)_playerT.position.x;
    }

    private void Update()
    {
        if (_move.enabled && _playerT.position.x >= 5 && !pauseStatus)
        {
            _currentPlayerPost = (int)_playerT.position.x;
            score = _currentPlayerPost - _scoreMin;

            scoreText.text = score.ToString()+"m";
        }


        if (Input.GetKey(KeyCode.Escape) && !pauseStatus && !once && _menu.ready)
        {
            once = true;
            if (_menu.startPressed)
            {
                
                _move.enabled = false;
                rb.velocity = Vector2.zero;
                rb.gravityScale = 0;
                pauseStatus = true;
                PauseText.gameObject.SetActive(true);
                //resumeButton.gameObject.SetActive(true);
                //panelCurrentH = shopOrPauseOverlay.offsetMax.x;
                shopOrPauseOverlay.gameObject.SetActive(true);
                PauseText.text = "Pause";
                Timing.RunCoroutine(movePanelFade(cg,true,true, zeroSet, .4f));
            }
            else
            {
                exitMenu.SetActive(true);
            }

            // endGame.SetActive(true);

        }

    }

    public void endGameSetup()
    {
        
        restartButton.SetParent(mainMenu);
        MainMenuButton.SetParent(mainMenu);
        scoreText.rectTransform.SetParent(mainMenu);
        mainMenu.gameObject.SetActive(true);
        scoreText.rectTransform.anchorMax = anchors[15];
        scoreText.rectTransform.anchorMin = anchors[14];
        scoreText.text ="Distance\n" + score.ToString()+"m";
        MainMenuButton.anchorMax = anchors[5];
        MainMenuButton.anchorMin = anchors[4];
        restartButton.anchorMax = anchors[13];
        restartButton.anchorMin = anchors[12];
        MainMenuButton.offsetMax = Vector2.zero;
        MainMenuButton.offsetMin = Vector2.zero;
        restartButton.offsetMax = Vector2.zero;
        restartButton.offsetMin = Vector2.zero;
        scoreText.rectTransform.offsetMax = Vector2.zero;
        scoreText.rectTransform.offsetMin = Vector2.zero;
        scoreText.gameObject.SetActive(true);

    }

    public void mainMenuorLeaderboard()
    {
        if (!_menu.startPressed)
        {
            
        }
        else
        {
            tomainMenu = true;
            if (gameOver)
            {
                Timing.RunCoroutine( movePanelFade(cgMainMenu, false, true, zeroSet, 0, .5f));
            }

            else
            {
                PauseText.text = "";
                Timing.RunCoroutine(movePanelFade(cg,true,true, panelCurrentH,1f,.5f));
            }
            

        }
        
        
    }
    public void restartorPlayGame()
    {
        if (!_menu.startPressed)
        {
           _menu.playButton(); 
        }

        else
        {
            restartGame = true;
            if (gameOver)
            {
                Timing.RunCoroutine( movePanelFade(cgMainMenu, false, true, zeroSet, 0, .5f));
            }

            else
            {
                PauseText.text = "";
                Timing.RunCoroutine(movePanelFade(cg,true,true, panelCurrentH,1f,.5f));
            }
   
        }
       

    }

    public void shopBackfunction()
    {
        Timing.RunCoroutine( movePanelFade(cgMainMenu, false, true, panelCurrentH, 1f));
    }

    public void unPauseorShop()
    {
        if (!_menu.startPressed)
        {
            
            Timing.RunCoroutine( movePanelFade(cgMainMenu, false, true, zeroSet, 0));
        }
        else
        {
            
            Timing.RunCoroutine(pauseCountDown());
        }
        
    }
    public IEnumerator<float> pauseCountDown()
    {
        Timing.RunCoroutine(movePanelFade(cg,true,false, panelCurrentH, .4f,.3f));
        countDown = 3;
        //resumeButton.gameObject.SetActive(false);
       
        pauseStatus = false;
        
        PauseText.rectTransform.anchorMax = anchors[8];
        PauseText.rectTransform.anchorMin = anchors[9];
        PauseText.rectTransform.offsetMax = Vector2.zero;
        PauseText.rectTransform.offsetMin = Vector2.zero;
        PauseText.text = " ";
        yield return Timing.WaitForSeconds(.5f);
        while (true)
        {
            PauseText.text = countDown.ToString();
            yield return Timing.WaitForSeconds(.5f);
            countDown--;
            
            
            if (countDown == 0)
            {
                _move.enabled = true;
                cg.alpha = 0;
                rb.gravityScale = 2.3f;
                shopOrPauseOverlay.gameObject.SetActive(false);
                PauseText.rectTransform.anchorMax = anchors[7];
                PauseText.rectTransform.anchorMin = anchors[6];
                PauseText.rectTransform.offsetMax = Vector2.zero;
                PauseText.rectTransform.offsetMin = Vector2.zero;
                once = false;
                PauseText.gameObject.SetActive(false);
                overlay.SetActive(false);
                break;
            }
        }

    }

    
    public IEnumerator<float> movePanelFade(CanvasGroup canvas ,bool horizontal,bool fading , float goal,float target,float lerpTime = .5f)
    {
        timing =0 ;
        alphaValStart = canvas.alpha;
        overlay.SetActive(true);
        if (horizontal && _menu.startPressed)
        {
            panelCurrentH = shopOrPauseOverlay.offsetMax.x;
        }
        else if (!horizontal&&!_menu.startPressed)
        {
            panelCurrentH = shopOrPauseOverlay.offsetMax.y;
        }
        
       
        _timeStartedLerping = Time.time;
        timeSinceStarted = Time.time - _timeStartedLerping;
        timing = timeSinceStarted / lerpTime;
        while (true)
        {
            
            if (!horizontal && !once && _menu.startPressed)
            {
                once = true;
                endGameSetup();
                
            }
            timeSinceStarted = Time.time - _timeStartedLerping;
            timing = timeSinceStarted / lerpTime;

            if (fading)
            {
                currentVal = Mathf.Lerp(alphaValStart, target, timing);
                canvas.alpha = currentVal;
                if (gameOver && !restartGame)
                {
                    cgMainMenu.alpha = currentVal;
                }
            }


            
            if (horizontal && _menu.startPressed && !gameOver)
            {
                
                panelMoveH = Mathf.Lerp(panelCurrentH, goal, timing);
                shopOrPauseOverlay.offsetMax = new Vector2(panelMoveH,0);
                shopOrPauseOverlay.offsetMin =new Vector2(panelMoveH,0);

            }

            if (!horizontal&&!_menu.startPressed &&!gameOver)
            {
                                
                panelMoveH = Mathf.Lerp(panelCurrentH, goal, timing);
                shopOrPauseOverlay.offsetMax = new Vector2(0,panelMoveH);
                shopOrPauseOverlay.offsetMin =new Vector2(0,panelMoveH);
            }
            
            if (timing >= 1)
            {
                if (!pauseStatus && (gameOver) && !tomainMenu)
                {
                    if (!restartGame)
                    {
                        rb.simulated = false; 
                    }
                    else if (restartGame)
                    {
                        SceneManager.LoadScene(_scene.name);
                    }
                }
                else if (restartGame&& pauseStatus&& !tomainMenu)
                {
                    SceneManager.LoadScene(_scene.name);
                }
                else if (tomainMenu)
                {
                    SceneManager.LoadScene(_scene.name);
                }
                break;
            }

            yield return Timing.WaitForOneFrame;
        }

    }
    
    
   

}
