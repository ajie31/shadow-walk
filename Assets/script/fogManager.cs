﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class fogManager : MonoBehaviour
{
    public float gradientVal = 0.15f;
    public float recentGradient = 0.15f;
    private float timingIt = 00;
    private SpriteRenderer rend;
    private int frameCount = 5;
    private float timingFog = 00;
    private float randomValue = 000;
    private float RandomFogControll = 0.00f;
    [SerializeField] private  gameplayManager gM;

    private void Start()
    {
       // Shader.SetGlobalFloat("_increment",gradientVal);
        rend = GetComponent<SpriteRenderer>();
        rend.material.SetFloat("_range",gradientVal);
    }

    private void Update()
    {
        if (Time.frameCount % frameCount == 0)
        {
            
            timingIt += Time.deltaTime;
            
        }

        if (timingIt >= 7)
        {
            RandomFogControll = Random.value;
            timingIt = 0;
            if (RandomFogControll >= 0.9f)
            {
                randomValue = Random.Range(15, 70) / 100f;
            }
            else
            {
                randomValue = Random.Range(15, 30) / 100f;
            }
           
           
            Timing.RunCoroutine(fogThickness(randomValue));
        }
    }

    private float lerpUnclamped(float a, float b, float t)
    {
        return a + (b - a) * t;
    }
    private IEnumerator<float> fogThickness(float random)
    {
        
        
        while (true)
        {
            timingFog += 0.5f * Time.deltaTime; 
            gradientVal = lerpUnclamped(recentGradient, random, timingFog);
            rend.material.SetFloat("_range",gradientVal);

            if ( timingFog >= 1.0f || gM.gameOver)
            {
                recentGradient = gradientVal;
                timingFog = 0;
                break;
            }

          yield return  Timing.WaitForOneFrame;

        }

        
    }
}


