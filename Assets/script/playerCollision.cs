﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;

using UnityEngine;
using UnityEngine.UI;
using MEC;

public class playerCollision : MonoBehaviour
{
    //public GameObject endGame;
    private bool once = false;
    private RaycastHit2D hit;
    private float length;
    private Vector3 front;
     private Vector3 headPos ;
     //private Camera _mainCam;
    private Texture2D _texture;
    
   [SerializeField] private  gameplayManager gM;

    [SerializeField] private LayerMask layer;
   // [SerializeField]private DynamicLight _dynamicLight; 
    
   // [SerializeField]private DynamicLight _dynamicLight1;
    
    [SerializeField] private playerMove _move;
    [SerializeField]private CharacterController2D _controller2;
   // private Rigidbody2D rb;

    private void Start()
    {
        headPos = new Vector3(transform.position.x,transform.position.y + .2f);
        hit = Physics2D.Raycast(transform.position, front, .55f, layer);
        length = 0.000f;
        front = new Vector3(0,0,0);
       // _mainCam = Camera.main;
       // rb = GetComponent<Rigidbody2D>();

        
    }

    private void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.CompareTag("lights"))
        {
          //  _dynamicLight.LightRadius += 1.2f; 
          //  _dynamicLight1.LightRadius += 0.4f; 
            obj.gameObject.SetActive(false);
        }
        if (obj.CompareTag("fall") && !once)
        {
           
            _controller2.enabled = false;
            _move.enabled = false;
            gM.gameOver = true;
            
            once = true;
            //Timing.RunCoroutine(lastFrame());
           // endGame.SetActive(true);
            Timing.RunCoroutine(gM.movePanelFade(gM.cg,false,true,gM.zeroSet,1));
            
            
        }
    }

    private void Update()
    {
       
        if (!once)
        {
            headPos = new Vector3(transform.position.x,transform.position.y + .2f);
            front = transform.TransformDirection(Vector3.right ) *.55f;
            //Debug.DrawRay(headPos,front,Color.green);
            hit = Physics2D.Raycast(headPos,front,.55f,layer);
            if (hit.collider && !_controller2.crouchInfo)
            {
                _controller2.enabled = false;
                _move.enabled = false;
                gM.gameOver = true;
                once = true;
                
                //endGame.SetActive(true);
                Timing.RunCoroutine(gM.movePanelFade(gM.cg,false,true,gM.zeroSet,1));
               
                // Debug.Log("contact");
            }
        }
       
    }
  
  /*  IEnumerator<float> lastFrame()
    {
        yield return Timing.WaitForOneFrame;
        RenderTexture rt = new RenderTexture(_mainCam.pixelWidth, _mainCam.pixelHeight,8);
        _mainCam.targetTexture = rt;
        _texture = new Texture2D(_mainCam.pixelWidth,_mainCam.pixelHeight,TextureFormat.RGB24, false);
        _mainCam.Render();
        RenderTexture.active = rt;
        _texture.ReadPixels(_mainCam.pixelRect, 0, 0);
        _texture.Apply();
        _mainCam.targetTexture = null;
        RenderTexture.active = null;
        Destroy(rt);
       // yield return Timing.WaitForSeconds(2);
        _mainCam.cullingMask = 0;
        Sprite tempSprite = Sprite.Create(_texture,new Rect(0,0,_mainCam.pixelWidth, _mainCam.pixelHeight),new Vector2(0,0));
        
        endGame.GetComponent<Image>().sprite = tempSprite;
        endGame.SetActive(true);
        Timing.RunCoroutine(fade(1));
        
       // _mainCam.enabled = false;
    }*/

   

   /* private IEnumerator<float> lastFrame()
    {
        _mainCam.clearFlags = CameraClearFlags.Nothing;
        yield return Timing.WaitForOneFrame;
        _mainCam.cullingMask = 0;
    }*/
}
