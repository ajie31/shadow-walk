﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using TMPro;
using UnityEngine.UI;

public class mainMenu : MonoBehaviour
{

    //player atteched Component
    [SerializeField] private playerMove _move;
    
    private CharacterController2D _controller2D;
    private Rigidbody2D rb;
    
    private float timing = 0, _timeStartedLerping = 0, timeSinceStarted = 0, currentValOverlay = 0, currentValMainMenuOverlay = 0; 
    // canvas atteched component
    private gameplayManager gM;
    
    //original 
    private float alphaStart;
    private float alphaStart2;
    private bool restarting = false;
    [HideInInspector] public bool startPressed = false;
    [HideInInspector] public bool ready = false;

    private TextMeshProUGUI leaderboardBttText;
    private TextMeshProUGUI shopBttText;
    private TextMeshProUGUI startingBttText;
    private RectTransform Title;
    private Image startingColor;
    [SerializeField] private TextMeshProUGUI textTitle;
    [SerializeField] private RectTransform mainMenuOverlay;
    [SerializeField] private RectTransform Overlay;
    [SerializeField] private RectTransform score;
    [SerializeField] private RectTransform leaderBoardButton;
    [SerializeField] private RectTransform shopButton;
    [SerializeField] private RectTransform startingButton;
    [SerializeField] private RectTransform MenuOverlay;
    [SerializeField] private RectTransform sfxBtt;
    [SerializeField] private RectTransform musicBtt;
    [SerializeField] private GameObject shopBackButton;
    [SerializeField] private GameObject exitMenu;
    [SerializeField] private CanvasGroup cgMainMenu;
 

    
    
    
    private void Start()
    {
        startPressed = false;
        ready = false;
        restarting = gameplayManager.restartGame;
        gM = GetComponent<gameplayManager>();
        _controller2D = _move.GetComponent<CharacterController2D>();
        Title = textTitle.GetComponent<RectTransform>();
        leaderboardBttText = leaderBoardButton.GetChild(0).GetComponent<TextMeshProUGUI>();
        shopBttText = shopButton.GetChild(0).GetComponent<TextMeshProUGUI>();
        startingBttText = startingButton.GetChild(0).GetComponent<TextMeshProUGUI>();
        rb = _move.GetComponent<Rigidbody2D>();
        Timing.RunCoroutine(mainMenuSetup(startPressed, .3f,1f));
        startingColor = startingButton.GetComponent<Image>();
    }



    public void playButton()
    {
       // startPressed = true;
        Timing.RunCoroutine(mainMenuSetup(true, 0f,0f,1f));
        
    }

    public void exitYes()
    {
        Application.Quit();
    }

    public void exitNo()
    {
        gM.once = false;
        exitMenu.SetActive(false);
    }

    //firstLoad Animation
    IEnumerator<float> mainMenuSetup(bool startButton,float target,float target2,float lerpingTime =1.5f)
    {
        alphaStart = gM.cg.alpha;
        alphaStart2 = cgMainMenu.alpha;

        if (!startButton)
        {
     
            yield return Timing.WaitForSeconds(3.8f);
            _move.enabled = false;
            _controller2D.enabled = false;
            rb.velocity = Vector2.zero;
            rb.gravityScale = 0;
        }
        timing = 0;
        _timeStartedLerping = Time.time;
        timeSinceStarted = Time.time - _timeStartedLerping;
        timing = timeSinceStarted / lerpingTime;

    
        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            timing = timeSinceStarted / lerpingTime;
           
            if (!restarting)
            {
                currentValOverlay = Mathf.Lerp(alphaStart, target, timing);
                gM.cg.alpha = currentValOverlay;
                currentValMainMenuOverlay = Mathf.Lerp(alphaStart2, target2, timing);
                cgMainMenu.alpha = currentValMainMenuOverlay;
            }
            else
            {
                currentValOverlay = Mathf.Lerp(alphaStart, 0, timing);
                gM.cg.alpha = currentValOverlay;
            }
           
           
            
            if (timing >= 1 && !startButton && !restarting)
            {
                ready = true;
                break;
            }
            else if ((startButton || restarting)&&timing >= 1 )
            {
                mainMenuOverlay.gameObject.SetActive(false);
                gM.once = false;
                leaderBoardButton.SetParent(MenuOverlay);
                shopButton.SetParent(MenuOverlay);
                startingButton.SetParent(MenuOverlay);
                Title.SetParent(Overlay);
                score.gameObject.SetActive(true);
                startingColor.color = gM.colorRestart;
                leaderBoardButton.anchorMax = gM.anchors[3];
                leaderBoardButton.anchorMin = gM.anchors[2];
                leaderBoardButton.offsetMax =gM.offsetUI[0];
                leaderBoardButton.offsetMin = gM.offsetUI[0];
                shopButton.anchorMax = gM.anchors[1];
                shopButton.anchorMin = gM.anchors[0];
                shopButton.offsetMax = gM.offsetUI[0];
                shopButton.offsetMin = gM.offsetUI[0];
                startingButton.anchorMax = gM.anchors[11];
                startingButton.anchorMin = gM.anchors[10];
                startingButton.offsetMax = gM.offsetUI[0];
                startingButton.offsetMin = gM.offsetUI[0];
                Title.anchorMax = gM.anchors[7];
                Title.anchorMin = gM.anchors[6];
                Title.offsetMax = gM.offsetUI[0];
                Title.offsetMin = gM.offsetUI[0];
                gM.shopOrPauseOverlay.anchorMax = gM.anchors[17];
                gM.shopOrPauseOverlay.anchorMin = gM.anchors[16];
                gM.shopOrPauseOverlay.offsetMax = gM.offsetUI[1];
                gM.shopOrPauseOverlay.offsetMin = gM.offsetUI[1];
                textTitle.text = "Pause";
                startingBttText.text = "Restart";
                leaderboardBttText.text = "Main Menu";
                shopBttText.text = "Resume";
                shopBackButton.SetActive(false);
                Title.gameObject.SetActive(false);
                Overlay.gameObject.SetActive(false);
                sfxBtt.gameObject.SetActive(false);
                musicBtt.gameObject.SetActive(false);
                gM.shopOrPauseOverlay.gameObject.SetActive(false);
                rb.gravityScale = 2.3f;
                _controller2D.enabled = true;
                _move.enabled = true;
                _move.movingSpeed = 27;
                if (restarting)
                {
                    gameplayManager.restartGame = false;
                }

                startPressed = true;
                ready = true;
                break;
            }

            
            
            yield return Timing.WaitForOneFrame;

        }
        
    }
}
