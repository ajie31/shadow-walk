﻿
using System.Collections.Generic;
using UnityEngine;
using MEC;
public class groundManager : MonoBehaviour
{
   // [SerializeField] private Transform spawnPost;
    [SerializeField] private Transform playerTrans;

   // [SerializeField] private planeWave _planeWave;
    [SerializeField] private generateMesh _generate;
    private float safeZone = 53f;
    private int amountTile = 3;
    private float tileMapLength = 30f;
    private List<GameObject> activeTile;
    private List<GameObject> activeObs;
    private float spawnX = -9.5f;
    private float addXforRope = 1;
    public bool starting = false;
    private int i = 0,j = 0;
    public int postSpawnControll = 2;
    private GameObject landSpawn,obs;
    
    private float heightRope;
    public int spawnControllObs;
    private float random,rotate,ybigTemple;
    

    private void Start()
    {
        spawnControllObs = 0;
        rotate = 0.0f;
        activeTile = new List<GameObject>();
        activeObs = new List<GameObject>();
        Timing.RunCoroutine(spawnatStart());
        //rand = Random.value;
        random = Random.value;
        
        
            //StartCoroutine(spawnatStart());
        var dist = (transform.position - Camera.main.transform.position).z;
        heightRope = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y;
        
        
    }

    private void Update()
    {
       if (playerTrans.position.x - safeZone > (spawnX - amountTile * tileMapLength) && starting )
        {
            spawnTile();
            deleteTile();
        }
    }
 
    void fixrotation(int x)
    {
        rotate = _generate.rotation[x];
        if (rotate < 0)
        {
            rotate = (180 + rotate) ;
        }
        else
        {
            rotate = (180 - rotate) * -1;
        }
    }
   public void spawnTile()
   {
       landSpawn = null;
       random = Random.value;
       if (random >= 0.9f)
       {
           landSpawn = poolingLand.current.getPooledBigTample();
           landSpawn.transform.position = new Vector3(spawnX+ 14.5f,-.5f,0); 
           spawnX += tileMapLength;
           landSpawn.SetActive(true);
        
       }
       else
       {
           
           landSpawn = poolingLand.current.getPooledLand();
           landSpawn.transform.position = new Vector3(spawnX ,landSpawn.transform.position.y ,0); 
           spawnX += tileMapLength;
           landSpawn.SetActive(true);
           _generate.createShape(landSpawn);
           
           if (_generate.spawnRope)
           {
               spawnObsRope(landSpawn.transform);
              // Debug.Log("rope");
           }
           
           for ( i = 0,j = 0; i < 2; i += 1)
           {
              
               random = Random.value;

               if (random >= .3f)
               {
                   random = Random.value;
                   fixrotation(j);
                   if (random > 0.9f  && postSpawnControll > 0)
                   {
                       spawnObsBatang(landSpawn.transform,_generate.spawnPostArray[i+i],rotate);
                       //spawnControllObs++;
                       postSpawnControll--;
                   }
                   else if (random > 0.7f &&  random <0.9f && postSpawnControll > 0)
                   {
                       spawnObsGantung(landSpawn.transform,_generate.spawnPostArray[i+i],rotate);
                       //  spawnControllObs++;
              
                       postSpawnControll--;
                   }
                   else if (random > 0.5f &&  random <0.7f && postSpawnControll > 0)
                   {
                       spawnObsTample(landSpawn.transform,_generate.spawnPostArray[i+i],rotate);
                       // spawnControllObs++;
          
                       postSpawnControll--;
                   }
               }
               
           
               j++;
           }
           
           
       }

      /* if (postSpawnControll != 2)
       {
           postSpawnControll++;
       }*/
       activeTile.Add(landSpawn);

    }

    void deleteTile()
    {
     /*   if (activeTile[0].transform.childCount > 0 && !activeTile[0].CompareTag("rope"))
        {
            activeObs[0].transform.SetParent(transform);
            activeObs[0].SetActive(false);
            activeObs.RemoveAt(0);
            spawnControllObs--;
        }
*/        
        spawnControllObs = activeTile[0].transform.childCount;
        if (spawnControllObs > 0 && !activeTile[0].transform.GetChild(0).CompareTag("ground"))
        {
           
            for (int k = 0; k < spawnControllObs ; k++)
            {
                postSpawnControll++;
                activeObs[0].transform.SetParent(transform);
                activeObs[0].SetActive(false);
                activeObs.RemoveAt(0);
                
            }

        }
        activeTile[0].SetActive(false);
        activeTile.RemoveAt(0);
        
    }

    IEnumerator<float> spawnatStart()
    {
        yield return Timing.WaitForSeconds(1);//new WaitUntil(() => poolingLand.current.ready == true);
        int counter = 0;
        while (true)
        {
            landSpawn = null;
            landSpawn = poolingLand.current.getPooledLand();
            landSpawn.transform.position = new Vector3(1* spawnX,landSpawn.transform.position.y,0);
            spawnX += tileMapLength;

       
            landSpawn.SetActive(true);
            _generate.createShape(landSpawn);
            //_planeWave.generateWave(landSpawn);
            activeTile.Add(landSpawn);
            counter++;
            if (counter == amountTile)
            {
                starting = true;
                break;
            }

            if (counter==2)
            {
                
              //  starting = true;
                playerTrans.GetComponent<Rigidbody2D>().simulated = true;
            }
            
            yield return Timing.WaitForSeconds(1.5f);
        }

        
      //  playerTrans.GetComponent<playerMove>().enabled = false;
        //playerTrans.GetComponent<CharacterController2D>().enabled = false;
        
    }

   private  void spawnObsBatang(Transform parent ,  Vector3 SpPost, float rotation)
   { 
       obs = poolingObs.current.GetPooledBatang();
       obs.transform.SetParent(parent);
       obs.transform.position = new Vector3(SpPost.x ,SpPost.y,0);
       
       obs.transform.eulerAngles = new Vector3(0,0,rotation);
       
       obs.SetActive(true);
       
       activeObs.Add(obs);
   }
   private  void spawnObsGantung(Transform parent, Vector3 SpPost, float rotation)
    { 
        obs = poolingObs.current.GetPooledGantung();
        obs.transform.SetParent(parent);
        obs.transform.position = new Vector3(SpPost.x ,SpPost.y + .7f,0);
      // Debug.Log(_generate.spawnPost.y);
        obs.transform.eulerAngles = new Vector3(0,0,rotation);
       
        obs.SetActive(true);
       
        activeObs.Add(obs);
    }
    private  void spawnObsTample(Transform parent, Vector3 SpPost, float rotation)
    { 
        obs = poolingObs.current.GetPooledTample();
        obs.transform.SetParent(parent);
        obs.transform.position = new Vector3(SpPost.x ,SpPost.y-.5f,0);
        // Debug.Log(_generate.spawnPost.y);
        obs.transform.eulerAngles = new Vector3(0,0,rotation);
       
        obs.SetActive(true);
       
        activeObs.Add(obs);
    }
    public  void spawnObsRope(Transform parent)
    { 
        obs = poolingLand.current.getPooledRope();
        obs.transform.position = new Vector3( _generate.spawnPostRope.x,heightRope-3,0);
            
        obs.transform.SetParent(parent);
        obs.SetActive(true);
        activeObs.Add(obs);
        postSpawnControll = -1;
        
    }
   
    

}
