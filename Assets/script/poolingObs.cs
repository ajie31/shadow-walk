﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class poolingObs : MonoBehaviour
{
    public static poolingObs current;
    public List<GameObject> Obstacles;
    public Transform parent;
    public int amountPoolingObs;
    private int counter = 0;
    private List<List<GameObject>> pooledObs = new List<List<GameObject>>();
    // Start is called before the first frame update

    private void Awake()
    {
        current = this;
    }

    void Start()
    {
         pooledObs.Add(new List<GameObject>());

        for (int i = 0; i < amountPoolingObs; i++)
        {
            GameObject obsBatang = (GameObject)Instantiate(Obstacles[0]);
            obsBatang.transform.SetParent(parent);
           
            //obsBatang.SetActive(false);
            pooledObs[0].Add(obsBatang);
        }

        pooledObs.Add(new List<GameObject>());

        for (int i = 0; i < amountPoolingObs; i++)
        {
            GameObject obsBatang = (GameObject)Instantiate(Obstacles[1]);
            obsBatang.transform.SetParent(parent);
           
            //obsBatang.SetActive(false);
            pooledObs[1].Add(obsBatang);
        }
        
        pooledObs.Add(new List<GameObject>());

        for (int i = 0; i < amountPoolingObs; i++)
        {
            GameObject obsBatang = (GameObject)Instantiate(Obstacles[2]);
            obsBatang.transform.SetParent(parent);
           
            //obsBatang.SetActive(false);
            pooledObs[2].Add(obsBatang);
        }

        Timing.RunCoroutine(falseDelay());

    }

    public GameObject GetPooledBatang()
    {
        for (int i = 0; i < pooledObs[0].Count; i++)
        {
            if (!pooledObs[0][i].activeInHierarchy)
            {
                return pooledObs[0][i];
            }
        }

        return null;
    }
    public GameObject GetPooledGantung()
    {
        for (int i = 0; i < pooledObs[1].Count; i++)
        {
            if (!pooledObs[1][i].activeInHierarchy)
            {
                return pooledObs[1][i];
            }
        }

        return null;
    }
    public GameObject GetPooledTample()
    {
        for (int i = 0; i < pooledObs[2].Count; i++)
        {
            if (!pooledObs[2][i].activeInHierarchy)
            {
                return pooledObs[2][i];
            }
        }

        return null;
    }

    private IEnumerator<float> falseDelay()
    {
        yield return Timing.WaitForSeconds(1f);
        while (true)
        {
            pooledObs[0][counter].SetActive(false);
            pooledObs[1][counter].SetActive(false);
            pooledObs[2][counter].SetActive(false);
            
            if (counter == pooledObs[0].Count -1)
            {
                counter = 0;
                break;
            }
            counter++;
            yield return Timing.WaitForSeconds(0.3f);
        }
        
        
    }


}
