﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class parallaxRandom : MonoBehaviour
{
    // Start is called before the first frame update
   
    private float length, startPos1,startPos2, temp,dist;
    [SerializeField] private List<Sprite> trees;
    public GameObject cam;
    private GameObject back1, back2;
    private List<SpriteRenderer> _renderers;
    private List<GameObject> bacgrounds;
    private int random;
    public float parallaxEffects;

    private float randomAppeareance;
    // Start is called before the first frame update
    void Start()
    {
        _renderers = new List<SpriteRenderer>();
        bacgrounds = new List<GameObject>();
        back1 = (transform.GetChild(0).gameObject);
        back2 = (transform.GetChild(1).gameObject);
        startPos1 = back1.transform.position.x;
        startPos2 = back2.transform.position.x;
        bacgrounds.Add(back1.transform.GetChild(0).gameObject);
        bacgrounds.Add(back1.transform.GetChild(1).gameObject);
        bacgrounds.Add(back1.transform.GetChild(2).gameObject);
        bacgrounds.Add(back2.transform.GetChild(0).gameObject);
        bacgrounds.Add(back2.transform.GetChild(1).gameObject);
        foreach (var render in bacgrounds)
        {
            _renderers.Add(render.GetComponent<SpriteRenderer>());
        }
        length = back2.GetComponent<SpriteRenderer>().bounds.size.x - .3f;
        temp = cam.transform.position.x * (1 - parallaxEffects);
        dist = cam.transform.position.x * parallaxEffects;
        Timing.RunCoroutine(cachingRenders());
        random = Random.Range(0, 6);
        randomAppeareance = Random.value;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        temp = cam.transform.position.x * (1 - parallaxEffects);
        dist = cam.transform.position.x * parallaxEffects;
        
       back1.transform.position = new Vector3(startPos1 + dist,transform.position.y,transform.position.z);
        back2.transform.position = new Vector3(startPos2 + dist,transform.position.y,transform.position.z);
        
        if (temp > startPos1 + length)
        {
            randomAppeareance = Random.value;
            if (randomAppeareance > 0.8f)
            {
                _renderers[0].gameObject.SetActive(false);
                _renderers[1].gameObject.SetActive(false);
                _renderers[2].gameObject.SetActive(false);
            }
            else
            {
                _renderers[0].gameObject.SetActive(true);
                _renderers[1].gameObject.SetActive(true);
                _renderers[2].gameObject.SetActive(true);
                random = Random.Range(0, trees.Count);
                _renderers[0].sprite = trees[random];
                random = Random.Range(0, trees.Count);
                _renderers[1].sprite = trees[random];
                random = Random.Range(0, trees.Count);
                _renderers[2].sprite = trees[random];
                startPos1 += length * 2;
            }
           
        }
        if (temp > startPos2 + length)
        {
            random = Random.Range(0, trees.Count);
            _renderers[3].sprite = trees[random];
            random = Random.Range(0, trees.Count);
            _renderers[4].sprite = trees[random];
            startPos2 += length * 2;
        }
        /* else if (temp < startPos - length)
         {
             startPos -= length;
         }*/
    }

    private IEnumerator<float> cachingRenders()
    {
        foreach (var render in _renderers)
        {
            foreach (var spriteIt in trees)
            {
                render.sprite = spriteIt;

                yield return Timing.WaitForOneFrame;
            }
        }
    }



}
