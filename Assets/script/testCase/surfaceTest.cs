﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using UnityEditor;

public class surfaceTest : MonoBehaviour
{
 [Range(0.1f, 28.0f)] public float heightScale = 5.0f;
    [Range(0.1f, 40.0f)] public float detailScale = 5.0f;
    private Mesh mesh;
    
    public GameObject ob;
    private Vector3[] vertices;
   [HideInInspector] public Vector3 spawnPost;
    [HideInInspector] public Vector3 spawnPost1;
    [HideInInspector] public Vector3 spawnPostRope;
    
    private int[] triangles;
    [HideInInspector]  public float rotation;
    public int xSize ;
    public int zSize ;
    // Start is called before the first frame update
    private EdgeCollider2D edgy;
    private Vector2[] pointing; 
    private int counter = 0;
   // private Vector3 checkRot;
    private Vector2[] uv;

    private void Start()
    {
        vertices =  new Vector3[(xSize + 1) * (zSize + 1)];
        uv = new Vector2[(xSize + 1) * (zSize + 1)];
        createShape(ob);
    }

    public void createShape(GameObject obj)
    {
        mesh = new Mesh();
        obj.GetComponent<MeshFilter>().mesh = mesh;
        
        vertices = new Vector3[(xSize + 1) * (zSize + 1)];
        uv = new Vector2[(xSize + 1) * (zSize + 1)];
        for (int i = 0,z = 0; z <= zSize; z++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                vertices[i] = new Vector3(x,z,0);
                CalculationMethod(i,z,obj);
                uv[i] = new Vector2((float)x / xSize, (float)z / zSize);
                
                i++;;
            }
        }


            triangles = new int[(xSize)*zSize*6];
            int vert =0;
            int tris = 0;

            for (int z = 0; z < zSize ; z++)
            {
            
                for (int x = 0; x < xSize; x++)
                {

                  if (x ==25 || x == 24 || x == 23 || x == 22 || x == 21 || x == 20 ||x == 19|| x == 18)
                    {
                        
                    }
                  else
                  {
                      triangles[tris+0] = vert + 0;
                      triangles[tris+1] = vert + xSize + 1;
                      triangles[tris+2] = vert + 1;
                      triangles[tris+3] = vert + 1;
                      triangles[tris+4] = vert + xSize + 1;
                      triangles[tris+5] = vert + xSize + 2;
                  }
                    
                
                    


                    vert++;
                    tris += 6;
                }
                Debug.Log(xSize);
                vert++;
            }

        

       
       updateMesh(mesh);
        mesh.uv = uv;
      Timing.RunCoroutine(drawingLine(obj));
    }

    void updateMesh(Mesh _mesh)
    {
        _mesh.Clear();

        _mesh.vertices = vertices;
        _mesh.triangles = triangles;
        
        _mesh.RecalculateNormals();
        _mesh.RecalculateBounds();
       //StartCoroutine(drawingLine());
    }

    void CalculationMethod(int i, int j,GameObject obj)
    {
      
            vertices[i].y = Mathf.PerlinNoise(    
                                (vertices[i].x + obj.transform.position.x) / detailScale,
                                (vertices[i].z + obj.transform.position.z) / detailScale)
                            * heightScale;
   
            vertices[i].y += j;
        
    }
    public Vector3 GetVertexWorldPosition(Vector3 vertex, Transform owner)
    {
        return owner.localToWorldMatrix.MultiplyPoint3x4(vertex);
    }
   
    private IEnumerator<float> drawingLine(GameObject obj)
    {
        edgy = obj.GetComponent<EdgeCollider2D>();
       
        //rotation = 0;
        counter = 0;
        
        pointing = edgy.points;
        //yield return new WaitForSeconds(.5f);
        while (true)
        {
            pointing[counter] = new Vector2(GetVertexWorldPosition(vertices[counter + ((xSize) * (zSize ))-22], transform).x,
                GetVertexWorldPosition(vertices[counter+((xSize) * (zSize ))-22], transform).y  );

            if (counter ==25)
            {
                break;
            }
            counter++;
            //yield return new WaitForSeconds(1.5f);
            yield return Timing.WaitForOneFrame;
        }

        spawnPost = obj.transform.TransformPoint(vertices[(counter +((xSize) * (zSize )-5))]);
        spawnPost1 = obj.transform.TransformPoint(vertices[(counter + ((xSize) * (zSize )-6))-5]) ;
        spawnPostRope = obj.transform.TransformPoint(vertices[(counter + ((xSize) * (zSize )-6))-6]) ;

        rotation = Mathf.Atan2(spawnPost1.y - spawnPost.y, spawnPost1.x - spawnPost.x) * 180 / Mathf.PI;


        if (rotation < 0)
        {
            rotation = (180 + rotation) ;
        }
        else
        {
            rotation = (180 - rotation) * -1;
        }
        edgy.points = pointing;

    }


    private void OnDrawGizmos()
     {
         if (vertices == null)
         {
             return;
         }
         for (int i = 0; i <= 102; i++)
         {
             
             Gizmos.DrawSphere(vertices[i], .1f);
             
         }
     }
}
