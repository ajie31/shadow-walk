﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;


public class fadeIn : MonoBehaviour
{
    private float timeSinceStarted,percentageComplete, currentVal,alphaValStart;
    private CanvasGroup cg;


    private void Start()
    {
        cg = GetComponent<CanvasGroup>();
        alphaValStart = cg.alpha;
        Timing.RunCoroutine(fade(1));
    }

    private IEnumerator<float> fade(float target, float lerpTime = 1f)
    {
      //  _timeStartedLerping = Time.time;
        timeSinceStarted = Time.deltaTime;
        percentageComplete = timeSinceStarted / lerpTime;
        
        while (true)
        {
            timeSinceStarted += Time.deltaTime *.8f ;
            //percentageComplete = timeSinceStarted / lerpTime;

            currentVal = Mathf.Lerp(alphaValStart,target, timeSinceStarted);
            

            cg.alpha = currentVal;

            if (timeSinceStarted >= 1f)
            {
                //Debug.Log("end");
               break;
                
                
            }

            yield return Timing.WaitForOneFrame;
        }
        
        
    }
    
   
}
