﻿
using System.Collections.Generic;
using MEC;
using UnityEngine;

public class generateMesh : MonoBehaviour
{
  [Range(0.1f, 28.0f)] public float heightScale = 5.0f;
    [Range(0.1f, 40.0f)] public float detailScale = 5.0f;
    public int xSize ;
    public int zSize ;
    private groundManager gM;
    public Vector3 spawnPostRope;
    public Vector3[] spawnPostArray;
    public float[] rotation;
    
     public bool spawnRope = false;
    
    private int[] triangles;
   
    private Mesh mesh;
    // public GameObject ob;
    private Vector3[] vertices;
    private int counter = 0;
    private EdgeCollider2D edgy;
    private Vector2[] pointing;
    private float rand;
    
    
   // private Vector3 checkRot;
    private Vector2[] uv;
    
    private void Start()
    {
        spawnPostArray = new Vector3[4];
        rotation = new float[2];
        gM = GetComponent<groundManager>();
        rand = Random.value;
        vertices =  new Vector3[(xSize + 1) * (zSize + 1)];
        uv = new Vector2[(xSize + 1) * (zSize + 1)];
        //createShape(ob);
    }

    public void createShape(GameObject obj)
    {
        rand = Random.value;
        if (rand >= .85f  &&  gM.postSpawnControll > 0 && gM.starting)
        {
            spawnRope = true;
            
        }
        
        mesh = new Mesh();
        obj.GetComponent<MeshFilter>().mesh = mesh;
        
        vertices = new Vector3[(xSize + 1) * (zSize + 1)];
        uv = new Vector2[(xSize + 1) * (zSize + 1)];
        for (int i = 0,z = 0; z <= zSize; z++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                vertices[i] = new Vector3(x,z,0);
                CalculationMethod(i,z,obj);
                uv[i] = new Vector2((float)x / xSize, (float)z / zSize);
                
                i++;;
            }
        }


            triangles = new int[(xSize)*zSize*6];
            int vert =0;
            int tris = 0;
            
            for (int z = 0; z < zSize ; z++)
            {
            
                for (int x = 0; x < xSize; x++)
                {

                    if ((x ==30 || x == 29 || x == 28 || x == 27 || x == 26|| x == 25|| x == 24 || x == 23) && spawnRope)
                    {
                        
                    }
                    else
                    {
                        triangles[tris+0] = vert + 0;
                        triangles[tris+1] = vert + xSize + 1;
                        triangles[tris+2] = vert + 1;
                        triangles[tris+3] = vert + 1;
                        triangles[tris+4] = vert + xSize + 1;
                        triangles[tris+5] = vert + xSize + 2;


                    }
                    


                    vert++;
                    tris += 6;
                }
                
                vert++;
            }

        

       
       updateMesh(mesh);
        mesh.uv = uv;
      Timing.RunCoroutine(drawingLine(obj));
    }

    void updateMesh(Mesh _mesh)
    {
        _mesh.Clear();

        _mesh.vertices = vertices;
        _mesh.triangles = triangles;
        
        _mesh.RecalculateNormals();
        _mesh.RecalculateBounds();
       //StartCoroutine(drawingLine());
    }

    void CalculationMethod(int i, int j,GameObject obj)
    {
      
            vertices[i].y = Mathf.PerlinNoise(    
                                (vertices[i].x + obj.transform.position.x) / detailScale,
                                (vertices[i].z + obj.transform.position.z) / detailScale)
                            * heightScale;
   
            vertices[i].y += j;
        
    }
    public Vector3 GetVertexWorldPosition(Vector3 vertex, Transform owner)
    {
        return owner.localToWorldMatrix.MultiplyPoint3x4(vertex);
    }
   
    private IEnumerator<float> drawingLine(GameObject obj)
    {
        edgy = obj.GetComponent<EdgeCollider2D>();
       
        //rotation = 0;
        counter = 0;
        spawnPostRope = obj.transform.TransformPoint(vertices[(28 + ((xSize) * (zSize )))]);

        spawnPostArray[0] = obj.transform.TransformPoint(vertices[(24 +((xSize) * (zSize )))]);
        spawnPostArray[1] =  obj.transform.TransformPoint(vertices[21 + ((xSize) * (zSize ))]);
        spawnPostArray[2] = obj.transform.TransformPoint(vertices[(14 +((xSize) * (zSize )))]);
        spawnPostArray[3] = obj.transform.TransformPoint(vertices[11 + ((xSize) * (zSize ))]);
        rotation[0] = Mathf.Atan2(spawnPostArray[1].y - spawnPostArray[0].y, spawnPostArray[1].x - spawnPostArray[0].x) * 180 / Mathf.PI; 
        rotation[1] = Mathf.Atan2(spawnPostArray[3].y - spawnPostArray[2].y, spawnPostArray[3].x - spawnPostArray[2].x) * 180 / Mathf.PI;
        pointing = edgy.points;
        //yield return new WaitForSeconds(.5f);
        while (true)
        {
            
            if ((counter ==30 || counter== 29 || counter == 28 || counter == 27 || counter == 26|| counter == 25 || counter == 24) && spawnRope)
            {
                pointing[counter] = new Vector2(GetVertexWorldPosition(vertices[19], transform).x,
                    GetVertexWorldPosition(vertices[26], transform).y  );  
            }

            else
            {
                pointing[counter] = new Vector2(GetVertexWorldPosition(vertices[counter + ((xSize) * (zSize ))-27], transform).x,
                    GetVertexWorldPosition(vertices[counter+((xSize) * (zSize ))-27], transform).y  +.7f);

            }

            if (counter ==30)
            {
                spawnRope = false;
                break;
            }
            counter++;
            yield return Timing.WaitForOneFrame;
        }



        edgy.points = pointing;

    }


  /*  private void OnDrawGizmos()
     {
         if (vertices == null)
         {
             return;
         }
         for (int i = 70; i <= 75; i++)
         {
             Gizmos.DrawSphere(vertices[i], .1f);
         }
     }*/
}
