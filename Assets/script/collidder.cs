﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

public class collidder : MonoBehaviour
{
    public bool ropeDone = false;
    private Rigidbody2D rb;
    private float _timeStartedLerping;
    private float timeSinceStarted;
    private float percentageComplete;
    private Vector2 tempVel;
   [SerializeField] private Transform target;
   // private Rigidbody2D rb;
    //public Transform endPoint;
    private Vector3 ending= new Vector3(2f,-11.3f,0);

    private void Start()
    {
        rb = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>();
       // tempVel = new Vector2(rb.velocity.x +5,rb.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.CompareTag("Player") && !ropeDone)
        {
            //ropeDone = _rope.getRopeAct();
             
           
            //rb.AddRelativeForce(new Vector2(5,5f),ForceMode2D.Impulse);
             rb.simulated = false;
             rb.velocity = new Vector2(5,5); 
             rb.transform.SetParent(transform);
            
             ropeDone = true;
                // rb.velocity = new Vector2(10,0); 
                // other.transform.SetParent(transform);
                //StartCoroutine(release(other));
           
             Timing.RunCoroutine(release(),Segment.FixedUpdate);
            // rb.velocity = tempVel;
        }
        
    }
    private Vector3 Lerp4(Vector3 start_value, Vector3 end_value, float t)
    {
        return new Vector3(
            start_value.x + (end_value.x - start_value.x) * t,
            start_value.y + (end_value.y - start_value.y) * t,
            start_value.z + (end_value.z - start_value.z) * t
        );
    }
    
    
    public IEnumerator<float> release( float lerpTime = 1f)
    {
         _timeStartedLerping = Time.time;
         timeSinceStarted = Time.time - _timeStartedLerping;
         percentageComplete = timeSinceStarted / lerpTime;
        
        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            Vector3 currentValue = Lerp4(transform.localPosition
                , target.localPosition, percentageComplete);
            

            transform.localPosition = currentValue;
            if (percentageComplete >= 0.25f &&ropeDone)
            {
                
                rb.simulated = true;
            }
            if (percentageComplete >= .5f)
            {
                rb.transform.SetParent(null);
                break;
                
            }

            yield return Timing.WaitForOneFrame;
        }
        
        Timing.RunCoroutine(goback(),Segment.FixedUpdate);
    }

    IEnumerator<float> goback(float lerpTime = 2f)
    {
        _timeStartedLerping = Time.time;
        timeSinceStarted = Time.time - _timeStartedLerping;
        percentageComplete = timeSinceStarted / lerpTime;
        
        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            Vector3 currentValue = Lerp4(transform.localPosition, ending, percentageComplete);


            transform.localPosition = currentValue;


            if (percentageComplete >= 1f)
            {
                ropeDone = false;
                break;
            }



            yield return Timing.WaitForOneFrame;
        }
        
    }
    

}
