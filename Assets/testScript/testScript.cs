﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testScript : MonoBehaviour
{
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    

    private void FixedUpdate()
    {
        if (Input.GetKey("right"))
        {
            rb.velocity += Vector2.right * 20 * Time.fixedDeltaTime;
        }
        if (Input.GetKey("left"))
        {
            rb.velocity += Vector2.left * 20 * Time.fixedDeltaTime;
        }
        if (Input.GetKeyDown("up"))
        {
            rb.velocity += Vector2.up * 150 * Time.fixedDeltaTime;
        } 
    }

  
  
}
